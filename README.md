![Minimal R Version](https://img.shields.io/badge/R%3E%3D-3.2.0-blue.svg)

# la app fue mudada a Shiny; es menos complicado que instalarla en distintas plataformas


# App para llenar los datos de los formatos de comprobación


__Descripción:__ La app extrae los datos requeridos para comprobación de gastos de los archivos xml de un directorio

__Uso:__ Poner todos los xml de una comprobación en una carpeta; en esa misma carpeta colocar el archivo de la app,
dar doble click y llenar los campos solicitados. 

![Imagen de la App](app.png)

El código creará un archivo excel con todos los campos requeridos de la factura:

![Archivo generado](ejemplo_excel_generado.png)

Además elimina folios duplicados y compara los códigos postales del _lugar de expedición_ contra una lista de códigos postales en un radio de 50 km.


<img src="codigos_3_digitos.png"  width="300" height="300">

__se requiere instalación previa del lenguaje R__
## Instalación en Linux:

Copiar y pegar en terminal __pegar es ctrl+shift+v__:

`wget https://gitlab.com/datamarindo/comprobacion_facturas/-/raw/master/comprobacion_source.R`

Dar permiso de ejecución:

`sudo chmod +x comprobacion_source.R`

Colocar en la carpeta con los xml, dar doble click, seleccionar "Run" (o "Ejecutar" si es en español) y llenar los campos

__SI NO ESTÁ INSTALADO R:__

_LINUX Para instalar R en terminal:_ `sudo apt update && sudo apt install r-base`

## MAC OS
_MACOS Para instalar R: https://cran.r-project.org/bin/macosx/R-4.0.2.pkg_

Instalar __xquartz__ https://xquartz.macosforge.org 
__Reiniciar para hacer xquartz el default__


En la `terminal` escribir:
`curl https://gitlab.com/datamarindo/comprobacion_facturas/-/raw/master/comprobacion_source.R > comprobacion_source.R`

Dar permiso de ejecución:
`sudo chmod +x comprobacion_source.R`

Decirle __sí__ a la instalación de __xtools__


## Instalación en windows

_Para instalar R, descargar y dar doble click al ejecutable:_ https://cran.r-project.org/bin/windows/base/R-4.0.2-win.exe

Ubicar la instalación de Rscript.exe, por lo general se instala en: C:\Users\MINOMBREDEUSUARIO\Program Files\R\R-4.0.2\bin\Rscript.exe, copiar la ruta.

Crear un archivo en bloc de notas que se llame `comprobacion.bat` y reemplazar la ruta por la copiada, el contenido del archivo será:

`"C:\users\MiNombreDeUsuario\Program Files\R\R-4.0.2\bin\Rscript.exe" https://gitlab.com/datamarindo/comprobacion_facturas/-/raw/master/comprobacion_source.R`


Al instalar los paquetes necesarios pedirá "Usar librería personal" y "Crear librería" (Aceptar ambos)

__En Windows se da doble click al archivo .bat__

